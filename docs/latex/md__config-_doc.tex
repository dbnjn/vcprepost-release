The primary input to the V\-C\-Pre\-Post suite is a human-\/readable simulation configuration file with a {\ttfamily $\ast$.config} extension. This sets the data conversion, simulation, and post-\/processing options, as well as auto-\/generates the {\ttfamily $\ast$.input} and {\ttfamily $\ast$.phys} ({\itshape not integrated yet}) files that Flow\-V\-C runs.

\section*{1. Basic config syntax rules}

Open the file in any text editor of your choice.

Each line begins with a three letter key as follows\-:
\begin{DoxyItemize}
\item {\ttfamily P\-R\-E\-:} indicates entries for pre-\/processing operations
\item {\ttfamily P\-S\-T\-:} indicates entries for post-\/processing operations
\item {\ttfamily F\-V\-C\-:} indicates entries for numerical simulation
\end{DoxyItemize}

Each line has either of the following syntax forms\-: ```bash K\-E\-Y\-: Descriptive Tag = value or K\-E\-Y\-: Descriptive Group Tag -\/ Descriptive Tag = value ```

Within each entry line, white spaces do not matter.

Comments can be added by including a line that starts with //. ```bash // This is a comment line ```

Each block, must have an end syntax such as follows\-: ```bash P\-R\-E\-: End Preprocessor = T\-R\-U\-E ```

\section*{2. Basic preprocessor block entries}

The first line typically is to enter the number of spatial dimensions your simulation domain has\-: ```bash P\-R\-E\-: Geometry Dimensions = integer-\/valued-\/entry ```

Then you need to enter the directory where the flow data (typically obtained from some C\-F\-D computation) resides, followed by the number of data files (corresponding to a sequence of time-\/steps from the simulations) that have been saved. {\bfseries Note\-:} All directory names throughout the config file must end with a trailing /. ```bash P\-R\-E\-: Data File Directory = string-\/valued-\/entry P\-R\-E\-: Data File Count = integer-\/valued-\/entry ``{\ttfamily  Data files are assumed to have a naming convention\-:}directory+prefix+\-I\-D+extension{\ttfamily . The next entries include the common filename prefix, and file extension. File extensions have to be either}.vtp, .vtu, .vtk{\ttfamily . }``bash P\-R\-E\-: Data File Prefix = string-\/valued-\/entry P\-R\-E\-: Data File Extension = string-\/valued-\/entry ``` The next entry in this block constitutes a tag for periodicity in data.
\begin{DoxyItemize}
\item If data is periodic in time, enter {\ttfamily T};
\item If data is periodic in space, enter {\ttfamily X};
\item If data is periodic in space and time, enter {\ttfamily T\-X};
\item If data is periodic in velocity, enter {\ttfamily V};
\item If data is periodic in velocity and time, enter {\ttfamily T\-V};
\item All else, data is not periodic
\end{DoxyItemize}

```bash P\-R\-E\-: Data Periodicity = string-\/valued-\/entry ```

The last entry in this block specifies the directory where the pre-\/processed binary files for Flow\-V\-C will be generated, along with a prefix that each file will be identified with. The naming scheme, again, is {\ttfamily directory+prefix+\-I\-D+extension}. Directory name must end with a trailing /. ```bash P\-R\-E\-: Bin File Directory = string-\/valued-\/entry P\-R\-E\-: Bin File Prefix = string-\/valued-\/entry ```

\section*{3. Block entries for data conversion}

Consider the following example for explanation of the {\ttfamily Data Conversion} group syntax. Time series flow data from a C\-F\-D simulation is saved as a sequence of {\ttfamily .vtu} unstructured grid datafiles named as {\ttfamily directory/prefix\-I\-D.\-vtu} (naming convention as described in Section 2 above). There are 51 files -\/ each at a different time step indexed by {\ttfamily I\-D} in the file name, and the actual time difference between successive data files is 0.\-01 sec. The {\ttfamily I\-D} index starts from 0 and ends at 50, incrementing in interval of 1. Based on these descriptions, entries in the config file would be as follows\-:

```bash P\-R\-E\-: Data Conversion -\/ File Type = vtu P\-R\-E\-: Data Conversion -\/ Index Start = 0 P\-R\-E\-: Data Conversion -\/ Index End = 50 P\-R\-E\-: Data Conversion -\/ Time Interval = 0.\-01 P\-R\-E\-: Data Conversion -\/ Index Interval = 1 ```

Running a Flow\-V\-C simulation requires the mesh coordinates, connectivity, adjacency data to be converted into a binary file format. This conversion is set by the following {\ttfamily T\-R\-U\-E/\-F\-A\-L\-S\-E} entry\-: ```bash P\-R\-E\-: Data Conversion -\/ Convert Meshfiles = T\-R\-U\-E ```

Similarly, nodal velocity data are also required to be converted into a binary file format, which is set by the following entries\-: ```bash P\-R\-E\-: Data Conversion -\/ Convert Datafiles = T\-R\-U\-E P\-R\-E\-: Data Conversion -\/ Data Fieldname = name of velocity data field (e.\-g. check from Paraview) ```

Surface normals are required to be converted into binary file format, to resolve wall boundary conditions. In addition, boundary cells are needed to be tagged in a binary file, to resolve collisional interactions for particles. These are set by the following entries\-: ```bash P\-R\-E\-: Data Conversion -\/ Convert Normals = T\-R\-U\-E P\-R\-E\-: Data Conversion -\/ Flip Normals = F\-A\-L\-S\-E P\-R\-E\-: Data Conversion -\/ Convert Boundaryflags = T\-R\-U\-E ```

The final step in conversion of flow data files is computing the bounding boxes for the flow domain. This is set by the following {\ttfamily T\-R\-U\-E/\-F\-A\-L\-S\-E} entry\-: ```bash P\-R\-E\-: Data Conversion -\/ Bounding Box = T\-R\-U\-E ```

\section*{4. Block entries for mesh based particle seeding}

Particle seeding is a critical part of running Lagrangian analysis. Configuration of seeding and particle initialization is achieved in two pre-\/processor group entries -\/ {\ttfamily Initialize Particles} and {\ttfamily Initialize Grid}. The former is for seeding particles based of an external surface mesh, the latter is for auto-\/generating an xyz Cartesian grid based on specified bounding boxes to seed particles. Mesh based particle seeding can be toggled using the following {\ttfamily T\-R\-U\-E/\-F\-A\-L\-S\-E} entry\-: ```bash P\-R\-E\-: Initialize Particles = T\-R\-U\-E ```

The name of the directory and the name of the file containing the mesh used for seeding the particles are specified using the following entries\-: ```bash P\-R\-E\-: Initialize Particles -\/ Input Directory = string-\/valued-\/entry P\-R\-E\-: Initialize Particles -\/ Seed Meshfile = string-\/valued-\/entry ```

The name of the directory and the vtk file where the resultant seeded/initialized particle locations are written to, are specified using the following entries\-: ```bash P\-R\-E\-: Initialize Particles -\/ Output Directory = string-\/valued-\/entry P\-R\-E\-: Initialize Particles -\/ Particle file = string-\/valued-\/entry ```

For finite size particle calculations, a non-\/zero particle radius can be specified\-: ```bash P\-R\-E\-: Initialize Particles -\/ Particle Radius = float-\/valued-\/entry ``` {\bfseries Note\-:} This is a valid radius for both mesh-\/based and grid-\/based particle seeding algorithms.

For cases where seeded particles are not inside (convex check) of the domain, you may need to flip the seeding direction as follows\-: ```bash P\-R\-E\-: Initialize Particles -\/ Flip Normals = T\-R\-U\-E ```

Often, prior to computing a large sample of particles, a smaller set of particles need to be computed to check whether results seem physically consistent, or if any configuration entry needs to be tweaked. This can be achieved by using a sub-\/sampling algorithm that requires a frequency parameter to be set (i.\-e. N where 1 out of every N samples are selected). The corresponding entries are\-: ```bash P\-R\-E\-: Initialize Particles -\/ Seed Subsample = T\-R\-U\-E P\-R\-E\-: Initialize Particles -\/ Subsample Frequency = integer-\/valued-\/entry ```

Finally, the seeded particles can be visualized by setting the following entry\-: ```bash P\-R\-E\-: Initialize Particles -\/ View Particles = T\-R\-U\-E ```

\section*{5. Block entries for grid based particle seeding}

Particle seeding based on Cartesian grids is relatively simple. This can be toggled on by the following entry\-: ```bash P\-R\-E\-: Initialize Particles On Grid = T\-R\-U\-E ```

Following this, particles are seeded within a specified rectangular/cuboidal bounded region, with specified resolution of number of points per direction. The X, Y, Z direction upper and lower bounds for the bounding box are specified as follows\-: ```bash P\-R\-E\-: Initialize Grid -\/ Mesh Bounds Xmin = float-\/valued-\/entry P\-R\-E\-: Initialize Grid -\/ Mesh Bounds Xmax = float-\/valued-\/entry P\-R\-E\-: Initialize Grid -\/ Mesh Bounds Ymin = float-\/valued-\/entry P\-R\-E\-: Initialize Grid -\/ Mesh Bounds Ymax = float-\/valued-\/entry P\-R\-E\-: Initialize Grid -\/ Mesh Bounds Zmin = float-\/valued-\/entry P\-R\-E\-: Initialize Grid -\/ Mesh Bounds Zmax = float-\/valued-\/entry ```

The corresponding resolution or number of points along each direction are specified as follows\-: ```bash P\-R\-E\-: Initialize Grid -\/ Grid Resolution X = integer-\/valued-\/entry P\-R\-E\-: Initialize Grid -\/ Grid Resolution Y = integer-\/valued-\/entry P\-R\-E\-: Initialize Grid -\/ Grid Resolution Z = integer-\/valued-\/entry ```

\section*{6. Basic block entries for Flow\-V\-C simulation}

Flow\-V\-C simulations are run using a {\ttfamily .input} file where numerical integration and simulation parameters are configured. Within the V\-C\-Pre\-Post-\/\-Flow\-V\-C suite, this input file is autogenerated. The filename (including full directory path if different from root folder) is set as follows\-: ```bash F\-V\-C\-: Project Input Filename = string-\/valued-\/entry ``` The remaining entries for Flow\-V\-C input are configured in specific blocks. Not all values need to be configured, depending upon the type of simulation being conducted.

\section*{7. Fluid property entries}

Fluid viscosity, density, and the x,y,z vector components of gravity are set in the following entries. These are required for cases where the finite size particle computations are needed. ```bash F\-V\-C\-: Fluid Properties -\/ Viscosity = float-\/valued-\/entry F\-V\-C\-: Fluid Properties -\/ Density = float-\/valued-\/entry F\-V\-C\-: Fluid Properties -\/ Gravity X = float-\/valued-\/entry F\-V\-C\-: Fluid Properties -\/ Gravity Y = float-\/valued-\/entry F\-V\-C\-: Fluid Properties -\/ Gravity Z = float-\/valued-\/entry ```

\section*{8. Timing and integration entries}

The first group of entries is for timing the simulation and outputs. Assume that the integrations start at time {\ttfamily T0}, and we need to dump {\ttfamily Nout} output files at an interval of {\ttfamily Dt} seconds. Total simulation or integration duration is\-: {\ttfamily Nout$\ast$\-Dt}; and time goes until {\ttfamily T0 + Nout$\ast$\-Dt}. These entries are configured in the following\-: ```bash F\-V\-C\-: Output Timing -\/ Simulation Start = T0 F\-V\-C\-: Output Timing -\/ Number Of Timepoints = Nout F\-V\-C\-: Output Timing -\/ Time Interval = Dt ```

The second group of entries is for setting up the parameters for numerical integration of the particle trajectories. The integration scheme choices are as follows\-:


\begin{DoxyItemize}
\item Type = 0\-: Forward Euler integration method
\item Type = 1\-: Fourth order Runge-\/\-Kutta method
\item Type = 2\-: Runge-\/\-Kutta-\/\-Fehlberg adaptive integration scheme
\end{DoxyItemize}

This can be set as follows\-: ```bash F\-V\-C\-: Time Integration -\/ Integration Type = 0/1/2 ```

Once configured, the time-\/step size and integration accuracy for the chosen scheme can be set using the following entries\-: ```bash F\-V\-C\-: Time Integration -\/ Time Step = float-\/valued-\/entry F\-V\-C\-: Time Integration -\/ Integration Accuracy = float-\/valued-\/entry ```

For the adaptive integration scheme, the upper and lower bounds of the error-\/adapted time-\/step size can be specified in the following entries\-: ```bash F\-V\-C\-: Time Integration -\/ Minimum Stepsize = float-\/valued-\/entry F\-V\-C\-: Time Integration -\/ Maximum Stepsize = float-\/valued-\/entry ```

For F\-T\-L\-E computations, the choice between forward and backward integration can be made by setting the following entry to either 1 or -\/1\-: ```bash F\-V\-C\-: Time Integration -\/ Time Direction = 1/-\/1 ```

For numerical integration, the particle interactions with the wall is handled by a wall boundary condition term. This boundary condition term is activated by setting the first entry below to 1, and then choosing a scaling parameter that is between 0.\-0 and 1.\-0 in the second entry\-: ```bash F\-V\-C\-: Time Integration -\/ Normal Correction = 0/1 F\-V\-C\-: Time Integration -\/ Normal Scaling = 0.\-0 -\/ 1.\-0 ```

For cases where particle exits the flow domain, numerical integration can be continued if desired by extrapolating velocities and positions outside the domain. For this set the following entry to 1. ```bash F\-V\-C\-: Time Integration -\/ Compute Extrapolation = 0 ```

\section*{9. Block entries for F\-T\-L\-E computation}

F\-T\-L\-E computation mode can be toggled on/off by setting the following entry to {\ttfamily T\-R\-U\-E/\-F\-A\-L\-S\-E}. ```bash F\-V\-C\-: Compute F\-T\-L\-E = T\-R\-U\-E/\-F\-A\-L\-S\-E ```

If the F\-T\-L\-E computation requires generation of a Cartesian mesh over which F\-T\-L\-E will be computed, with mesh parameters specified in the grid-\/based particle seeding block above, set the first entry in the following to 1. If the computation instead reads in from an external grid-\/seeded file, set the first entry in the following to 0. The second line sets the name of the binary file where the Cartesian grid is read from (if first entry is 0) or dumped into (if first entry is 1)\-: ```bash F\-V\-C\-: F\-T\-L\-E Computation -\/ Generate Meshfile = 1/0 F\-V\-C\-: F\-T\-L\-E Computation -\/ Initialization File = string-\/valued-\/entry ```

The total duration for tracer integrations for F\-T\-L\-E computations can be set as follows\-: ```bash F\-V\-C\-: F\-T\-L\-E Computation -\/ Integration Duration = float-\/valued-\/entry ``{\ttfamily  $\ast$$\ast$\-Note\-:$\ast$$\ast$ This duration should be consistent with the}Nout$\ast$\-Dt` values in Section 8.

The file prefix for the F\-T\-L\-E output files where the F\-T\-L\-E field data is dumped is set in the following entry\-: ```bash F\-V\-C\-: F\-T\-L\-E Computation -\/ Output Prefix = string-\/valued-\/entry ```

\section*{10. Block entries for tracer computations}

Tracer computation mode can be toggled on/off by setting the following entry to {\ttfamily T\-R\-U\-E/\-F\-A\-L\-S\-E}. ```bash F\-V\-C\-: Compute Tracers = T\-R\-U\-E/\-F\-A\-L\-S\-E ```

Tracers from the initialized/seed locations can be released into the flow domain in two different ways, by setting the following entry to 0 or 1.


\begin{DoxyItemize}
\item 1\-: Release tracers at fixed instants in time
\item 2\-: Release tracers in staggered manner continuously based on flow rate
\end{DoxyItemize}

```bash F\-V\-C\-: Tracer Computation -\/ Tracer Release Strategy = 1/0 ``{\ttfamily  For staggered release, the release starts at the value entered for}Simulation Start{\ttfamily in Section 8, and continues for a duration specified by the following entry\-: }``bash F\-V\-C\-: Tracer Computation -\/ Staggered Release Duration = float-\/valued-\/entry ```

For fixed instants of release, we can set a number of instants for launching particles, spaced at a specific interval in time over the entire simulation duration by setting the first and second entries in the following respectively. If the number is set to 1, and interval set to 0, then there is only one launch of tracers. The tracer release starts at the value entered for {\ttfamily Simulation Start} in Section 8. ```bash F\-V\-C\-: Tracer Computation -\/ Release Instant Count = integer-\/valued-\/entry F\-V\-C\-: Tracer Computation -\/ Release Instant Intervals = float-\/valued-\/entry ```

Finally, a global maximum integration duration is specified for particles that may remain stuck in swirling structures\-: ```bash F\-V\-C\-: Tracer Computation -\/ Integration Duration = float-\/valued-\/entry ```

\section*{11. Basic postprocessor block entries}

The post-\/processor can be activated to convert simulation output data from Flow\-V\-C into V\-T\-K format by toggling the following entries in the post-\/processor block\-: ```bash P\-S\-T\-: Convert Particle Data = T\-R\-U\-E ```

The underlying data conversion relies on identifying the type of data being converted from the simulation output. The different recognized types are as follows\-:
\begin{DoxyItemize}
\item 0\-: Tracer position data
\item 1\-: Scalar unstructured node data
\item 2\-: Vector unstructured node data
\item 3\-: Scalar unstructured element data
\item 4\-: Vector unstructured element data
\item 5\-: Scalar Cartesian node data
\item 6\-: Vector Cartesian node data
\item 7\-: Scalar Cartesian element data
\item 8\-: Vector Cartesian element data
\end{DoxyItemize}

This can be set by the following entry\-: ```bash P\-S\-T\-: Particle Conversion -\/ Data Type = integer-\/valued-\/entry ``` For reference, the two most common modalities are\-:
\begin{DoxyItemize}
\item Tracer particle integration\-: Type = 0
\item F\-T\-L\-E Field data\-: Type = 5
\end{DoxyItemize}

The generated simulation output binary files have a typical naming scheme\-: {\ttfamily directory+fileprefix.I\-D.\-bin}. The {\ttfamily directory} name where the simulation output are dumped, along with the file prefix, starting I\-D, final I\-D, and intervals in which the I\-D increments can all be set using the following entries\-: ```bash P\-S\-T\-: Particle Conversion -\/ Data Directory = string-\/valued-\/entry P\-S\-T\-: Particle Conversion -\/ File Prefix = string-\/valued-\/entry P\-S\-T\-: Particle Conversion -\/ Index Start = integer-\/valued-\/entry P\-S\-T\-: Particle Conversion -\/ Index End = integer-\/valued-\/entry P\-S\-T\-: Particle Conversion -\/ Index Interval = integer-\/valued-\/entry ```

\section*{12. Questions/comments\-:}

Email \href{mailto:debanjan@Colorado.Edu}{\tt debanjan@\-Colorado.\-Edu} for any questions or comments. 