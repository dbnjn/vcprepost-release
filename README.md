# VCPrePost

VCPrePost is a Python based library that serves as an interface to conduct a range of Lagrangian flow analysis computations on a variety of flow-data formats using the C library FlowVC.

## 1. Software Requirements

The package requires the following software dependencies:

* GNU C compiler
* Python3 installation
* Scientific Python stack: Numpy, Scipy, Matplotlib
* Python subprocess module
* VTK Library with Python bindings

For Mac users, it is recommended to install all Python dependencies through HomeBrew. For further information you can visit the following link: https://docs.brew.sh/Homebrew-and-Python

For Windows users, it is recommended to install all Python dependencies through Anaconda. For further information you can visit the Anaconda documentation pages: https://docs.anaconda.com/anaconda/user-guide/tasks/install-packages/

For Linux users, use the Advanced Packaging Tool (e.g. `sudo apt-get ....`).

At this stage, VCPrePost is packaged independently from `FlowVC`. Hence, a final prerequisite of running simulations with the combined VCPrePost-FlowVC suite, as described here, you need to also have an installed version of `FlowVC`. Follow the instructions as below:
- Download FlowVC source/binary from: https://shaddenlab.berkeley.edu/software.html
- Install/compile (if source downloaded)
- Copy the compiled executable in the `VCPrePost/src/` directory

## 2. Installation Instructions

**Note for Linux and Mac users**
Download or clone the library. Then navigate to the root directory and run the makefile:
```bash
cd VCPrePost/src/
make all
```

**Note for Windows Users with Anaconda Distribution**
Here are some workarounds using `Cygwin` in Windows platforms for completing the installation:
* Download and install `Cygwin` for your computer locally.
* Copy the `cygwin1.dll` from the `bin` directory in the `cygwin` install folder into the `Windows` directory in `C:/` (this helps address any installation errors during the next steps).
* Within `Cygwin` navigate to the root directory for `VCPrePost`: `cd VCPrePost/src/`
* Type: `make all`
* Once installed, assuming you are working with the Anaconda distribution, open up the conda python terminal.

## 3. Usage

You can use VCPrePost in three different forms

### a. Simulation Pipeline Mode
This mode runs a complete particle simulation using FlowVC based on a single line command that is interpreted and executed by VCPrePost. Simply navigate to project directory, set up the project `.config` file, and run.

### b. Pre-Post Mode
This mode simply conducts the preprocessing steps and the postprocessing steps for a given simulation, assuming the user will run the FlowVC simulation separately. This is typically recommended for HPC cluster based computations.

### c. Developer Mode
In developer mode, the main VCPrePost scripts are not executed. Instead, the python libraries are used as API's/Modules to develop custom pre-processing, and post-processing scripts. Refer to the Doxygen style documentation of the Python codebase in the sub-folder `docs`.

### d. Recommended Project Directory Structure
It is recommended that a common project directory structure is maintained as follows:
- root
  - flow-data-Directory
  - fvc-bin-files
  - fvc-outputs
  - simulation-inputs

## 4. Examples

The best way to get started is to work on the packaged examples provided along with this library, located in the `examples` directory. There are two examples provided. The folder `doublegyre` contains an example computation of FTLE fields for a canonical double gyre flow velocity field. The folder `cervical-ica` contains an example computation of tracer particle trajectories for a cervical segment of the human right internal carotid artery.

### a. Double Gyre FTLE Example

**Note:** We will use `VCPrePost` as the placeholder for the name of the directory including the full path where the package was installed. You will need to edit this accordingly.

**Note:** For Windows users, use forward slashes in your path names for files: such as `C:/cygwin64/Research/VCPrePost/test/inputs/` etc.

- Once installation is complete, navigate to the example directory `doublegyre`.
- Unzip the provided data directory `vtk.zip`.
- Create the folders `doublegyre/fvcbin` and `doublegyre/fvcout`.
- Navigate to the `doublegyre/input` directory, and open the provided file `FTLE_config.config`.
- Replace the correct pathname in `VCPrePost` for preprocessor entry `Data File Directory`:
```bash
PRE: Data File Directory  = VCPrePost/examples/doublegyre/vtk/
```
- Replace the correct pathname in `VCPrePost` for preprocessor entry `Bin File Directory`:
```bash
PRE: Bin File Directory   = VCPrePost/examples/doublegyre/fvcbin/
```
- Replace the correct pathname in `VCPrePost` for postprocessor entry `Data Directory`:
```bash
PST: Particle Conversion - Data Directory = VCPrePost/examples/doublegyre/fvcout/
```
- Replace the correct pathname in `VCPrePost` for FlowVC parameter entry `Project Input Filename`:
```bash
FVC: Project Input Filename = VCPrePost/examples/doublegyre/input/FTLE_sim.input
```
- Save this edited configuration file.
- Now open the terminal/command-prompt and execute the simulation pipeline mode version of the code by entering:
```bash
python3 VCPrePost/src/runSimPipelineMode.py VCPrePost/examples/doublegyre/input/FTLE_config.config
```

### b. Tracer Particle Example

**Note:** We will use `VCPrePost` as the placeholder for the name of the directory including the full path where the package was installed. You will need to edit this accordingly.

**Note:** For Windows users, use forward slashes in your path names for files: such as `C:/cygwin64/Research/VCPrePost/test/inputs/` etc.

- Once installation is complete, navigate to the example directory `cervical-ica`.
- Unzip the provided data directory `velocity.zip`
- Create the folders `cervical-ica/fvcbin` and `cervical-ica/fvcout`.
- Navigate to the `cervical-ica/input` directory, and open the provided file `trace_config.config`.
- Replace the correct pathname in `VCPrePost` for preprocessor entry `Data File Directory`:
```bash
PRE: Data File Directory  = VCPrePost/examples/cervical-ica/vtk/
```
- Replace the correct pathname in `VCPrePost` for preprocessor entry `Bin File Directory`:
```bash
PRE: Bin File Directory   = VCPrePost/examples/cervical-ica/fvcbin/
```
- Replace the correct pathname in `VCPrePost` for postprocessor entry `Data Directory`:
```bash
PST: Particle Conversion - Data Directory = VCPrePost/examples/cervical-ica/fvcout/
```
- Replace the correct pathname in `VCPrePost` for FlowVC parameter entry `Project Input Filename`:
```bash
FVC: Project Input Filename = VCPrePost/examples/cervical-ica/input/FTLE_sim.input
```
- Save this edited configuration file.
- Now open the terminal/command-prompt and execute the simulation pipeline mode version of the code by entering:
```bash
python3 VCPrePost/src/runSimPipelineMode.py VCPrePost/examples/cervical-ica/input/trace_config.config

## License
Distribution of this package is under [BSD-2 License](https://opensource.org/licenses/BSD-2-Clause)

## Questions?
If you have any questions, if you would wish to use this package (and FlowVC) for your research, or if you would like to contribute to this project, feel free to email at: debanjan@Colorado.Edu.
