#---------------------------------------------------------------------------------
## @file runPostProcessorMode.py
#  @author Debanjan Mukherjee, University of Colorado Boulder
#
# @brief This is a master script that runs the pre-processing steps for running 
# a tracer particle integration simulation using FlowVC developed by S.C. Shadden
# 
# @note Usage: To run this file simply type on a console the following command
# python3 runPostProcessorMode.py <configurationfile>
# where the <configurationfile> is a file with a .config extension
#
#--------------------------------------------------------------------------------

#---------------------
# BEGIN MODULE IMPORTS
#---------------------
import sys, os

try:
    import configPrePost as CP
except ImportError:
    sys.exit("Could not import module configPrePost. Check configuration.")

try:
    import preProcessorFlowVC as PRE
except ImportError:
    sys.exit("Could not import module preProcessorFlowVC. Check configuration.")

try:
    import postProcessorFlowVC as POST
except ImportError:
    sys.exit("Could not import module postProcessorFlowVC. Check configuration.")
    
try:
    import subprocess
except ImportError:
    sys.exit("Could not import module subprocess. Check Python installation.")
#-------------------
# END MODULE IMPORTS
#-------------------

numArgs = len(sys.argv)

#----------------------------------
# read the input configuration file
#----------------------------------
if numArgs == 1:
    
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

    print("This VCPrePost script needs to be executed as follows:")
    print("python3 runPostProcessorMode.py configuration.config compute-mode")
    print("configuration.config is the input config data file")
    print("compute-mode is a choice between ftle/tracers")
    sys.exit()

elif numArgs == 3:
    
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

    configFile  = str(sys.argv[1])
    computeMode = str(sys.argv[2])

else:
    
    sys.exit("Invalid Number of Arguments at Command Line")

#--------------------------------------------------
# create the postprocessor confguration data object
#--------------------------------------------------
print("Reading Configuration File")
postConfig   = CP.configDataPostProcessor(configFile)
postConfig.setConfigData()
postConfig.printConfigData()

appPath = os.path.dirname(os.path.realpath(__file__))
    
if not appPath.endswith('/'):
    appPath = appPath + '/'

dataType = postConfig.getParticleDataType()

if dataType == 0:
    filePrefix = postConfig.getParticleDataDirectory() + postConfig.getParticleFilePrefix()
    POST.scriptBinToVtk(filePrefix, postConfig.getParticleIndexStart(), postConfig.getParticleIndexStop(), a_Bin2VtkPath=appPath)

